### KROME bootcamp 2021 exercise repository
-------------------------

Here you can find exercise talks, text, and solutions (see subfolders).


The Wiki for PROTO can be found [here](https://bitbucket.org/tgrassi/krome_bootcamp_2021_ex/wiki/Home).

To get PROTO, clone this repository by typing this in your shell/terminal (in the path where you want to put this directory):

```
git clone https://bitbucket.org/tgrassi/krome_bootcamp_2018_ex.git
```
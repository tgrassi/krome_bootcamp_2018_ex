from Kplot import Kplot as kp

# path argument points to the folder where you have
# evolution.dat from the first problem (e.g. build)
# and radiation.dat and density.dat from PROTO exercise.
# NOTE: the path is relative to where you have this plot.py file.
# Alternatively, move plot.py and Kplot.py in the mentioned folders
# and do not set any path argument, i.e. p = kp()
p = kp(path="../proto/")

# change plot1 with plot2, plot3, ... to produce further plots
p.plot1()


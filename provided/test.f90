!**************
! This code evolve the abundances of a chemical network
! for a time tend using incremental time-steps of size dt
!**************
program test
  use krome_main
  use krome_user
  implicit none
  real*8,parameter::secondsPerYear=365.*24.*3600.
  real*8::x(krome_nmols),n(krome_nmols),t,dt,Tgas
  real*8::rhogas,tend

  !initialize KROME
  call krome_init()

  !set CR ionization rate
  call krome_set_user_crate(###)

  !max simulation time
  tend = 1d6*secondsPerYear

  !gas temperature, K
  Tgas = 1d3

  !total density, g/cm3
  rhogas = 2d-16

  !initialization (mass fractions)
  x(:) = 0d0
  x(krome_idx_H) = ###
  x(krome_idx_H2) = ###
  x(krome_idx_O) = ###

  !convert mass fraction to number density
  n(:) = krome_x2n(x(:),rhogas)

  !open file where to read with header
  open(66,file="evolution.dat",status="replace")
  write(66,*) "#time "//trim(krome_get_names_header())

  !initial time-step, s
  dt = 0.1*secondsPerYear

  !init total time
  t = 0d0

  !loop on time
  do
     !increase time-step
     dt = dt * 1.1

     !solve chemistry with KROME
     call krome(n(:),Tgas,dt)

     !increase total time
     t = t + dt

     !dump output
     write(66,'(99E17.8e3)') t/secondsPerYear,n(:)

     !exit when end time reached
     if(t>tend) exit
  end do
  close(66)

  print *,"done, goodbye!"

end program test

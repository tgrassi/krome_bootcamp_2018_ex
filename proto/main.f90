program proto_main
  use proto_class
  use proto_commons
  implicit none
  type(proto)::p
  integer::fileUnitRadiation, fileUnitDensity
  integer::istep
  real*8,parameter::secondsPerYear=365.*24.*3600.
  real*8::rhog(ncells),r(ncells),dt,t,timeEnd

  !create object
  p = proto()

  !load options and initialize
  call p%init("namelist.dat")

  !end of simulation time (s)
  timeEnd = 1d5 * secondsPerYear
  !initial time-step (s)
  dt = 1d-4 * secondsPerYear

  !output density profile (and add header)
  open(newunit=fileUnitDensity, file="density.out", status="replace")
  write(fileUnitDensity, *) "#radius density"
  call p%dumpDensityProfile(fileUnitDensity, 0d0)

  !init total time (s)
  t = 0d0
  !get density (all cells)
  rhog(:) = p%getDensity()
  !get radius (all cells)
  r(:) = p%getRadius()

  !init chemistry
  call p%initChemistry()

  print *, "--------------"
  print *, "starting loop on time..."
  print *, "(it might take a while)"

  !open file to dump radiation (and add header)
  open(newunit=fileUnitRadiation, file="radiation.out", status="replace")
  write(fileUnitRadiation, *) "#time radius energy flux opacity"

  istep = 0
  !loop on time
  do
     !increase time-step
     dt = dt * 1.1
     !define radiation source
     call p%setRadiationSource()
     !propagate radiation
     call p%evolveRadiation()
     !evolve pseudo-hydrodynamics
     !call p%evolveHydro(dt)
     !solve chemistry (call krome)
     call p%solveChemistry(dt)
     !increase total time
     t = t + dt
     !dump radiation flux to file (open above)
     call p%dumpRadiationFlux(fileUnitRadiation, t)
     !save data to file
     call p%dumpDensityProfile(fileUnitDensity, t)

     !print some output on screen
     if(mod(istep,10)==0) print '(E12.3,F7.1,a1)',  t /secondsPerYear, &
          1d2 * log10(t) / log10(timeEnd), "%"

     !increse evolution step number
     istep = istep + 1
     !quit after timeEnd is reached
     if(t>=timeEnd) exit
  end do
  close(fileUnitRadiation)
  close(fileUnitDensity)

  print *,"everything done, goodbye!"

end program proto_main

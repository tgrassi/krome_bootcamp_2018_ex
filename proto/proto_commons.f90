module proto_commons
  !number of cells (grid points)
  integer,parameter::ncells=100
  !number of chemical species
  integer,parameter::nchemistry=0
  !number of energy bins (radiation)
  integer,parameter::nenergy=10
  !number of dust bins (total)
  integer,parameter::ndust=0

  !conversions
  real*8,parameter::msun2g=1.98855d33 !msun->g
  real*8,parameter::yr2s=365.*24.*3600. !yr->s
  real*8,parameter::g2msun=1d0/msun2g !g->msun
  real*8,parameter::s2yr=1d0/yr2s !s->yr
  real*8,parameter::erg2ev=6.24150913d11 !erg->eV
  real*8,parameter::ev2erg=1d0/erg2ev !eV->erg
  real*8,parameter::rsun2cm=6.96d10 !rsun->cm
  real*8,parameter::cm2rsun=1d0/rsun2cm !cm->rsun
  real*8,parameter::au2cm=1.496d13 !AU->cm

  !constants
  real*8,parameter::pi=acos(-1d0) !#
  real*8,parameter::gravity=6.674d-8 !cm3/g/s2
  real*8,parameter::pmass=1.67262158d-24 !g
  real*8,parameter::clight=2.99792458d10 !cm/s
  real*8,parameter::kboltzmann=1.380648d-16 !erg/K
  real*8,parameter::kboltzmann_ev=kboltzmann*erg2ev !eV/K
  real*8,parameter::hplanck=6.62607554d-27 !erg*s
  real*8,parameter::hplanck_eV=hplanck*erg2ev !eV*s

end module proto_commons
